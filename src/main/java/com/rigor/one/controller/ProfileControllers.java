package com.rigor.one.controller;

import com.google.gson.Gson;
import com.rigor.one.domain.Candidate;
import com.rigor.one.domain.Referrer;
import com.rigor.one.service.ProfileService;
import com.rigor.one.service.SocialLinksFetcher;
import com.rigor.one.util.APIResult;
import com.rigor.one.util.Helper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jenefar
 */
@RestController
public class ProfileControllers {

    @Autowired
    ProfileService profileService;

    private static final Logger logger = LoggerFactory.getLogger(ProfileControllers.class);
    Helper helper = new Helper();


    @RequestMapping(value = "/check", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    String home() {
        System.out.println("Method is called");
        List<Candidate> emails = profileService.getProfilesByJobId("1234");
        for (Candidate e : emails) {
            System.out.println(e.getEmail());
        }
        return "Working";
    }


    @RequestMapping(value = "/profile/{jobId}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    List<Candidate> home(@PathVariable String jobId) {
        System.out.println("Method is called");
        List<Candidate> candidates = profileService.getProfilesByJobId(jobId);

        return candidates;
    }

    @RequestMapping(value = "/upload/excel/{jobId}", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    ResponseEntity uploadExcelTracker(@RequestParam(value = "file") MultipartFile file, @PathVariable String jobId, @RequestParam String referrer) {
        logger.debug("File upload Method is called");
        APIResult apiResult = new APIResult();
        try {
            String uploadedFile = helper.uploadFileToLocal(file);
            List<Candidate> candidates = profileService.extractProfilesFromExcelTracker(uploadedFile);
            List<Candidate> existingCandidates = profileService.getProfilesByJobId(jobId);

            List<String> existingEmails = new ArrayList<>();
            List<String> duplicateEmails = new ArrayList<>();
            SocialLinksFetcher socialLinksFetcher = new SocialLinksFetcher();
            apiResult = new APIResult();
            for (Candidate c : existingCandidates) {
                existingEmails.add(c.getEmail().trim());
            }

            for (Candidate candidate : candidates) {
                if (!existingEmails.contains(candidate.getEmail())) {
                    candidate.setJobId(jobId);
                    candidate.set_id(helper.generateUniqueId());
                    candidate = socialLinksFetcher.fetchFullContactApiResults(candidate);
                    Referrer ref = new Gson().fromJson(referrer, Referrer.class);

                    candidate.setReferrer(ref);
                    profileService.saveProfile(candidate);
                } else {
                    duplicateEmails.add(candidate.getEmail());
                    logger.debug("duplicate profile:::::" + candidate.getEmail());
                }
            }

            if (duplicateEmails.size() > 0) {
                apiResult.setError("Duplicate profiles found");
                apiResult.setData(StringUtils.join(duplicateEmails, ','));
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new Gson().toJson(apiResult), HttpStatus.OK);
    }

    @RequestMapping(value = "/consultant/profile", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    ResponseEntity uploadProfile(@RequestBody Candidate candidate) {
        logger.debug("consultant profile upload Method is called");
        APIResult apiResult = new APIResult();
        try {
            List<Candidate> existingCandidates = profileService.getProfilesByJobId(candidate.getJobId());

            List<String> existingEmails = new ArrayList<>();
            SocialLinksFetcher socialLinksFetcher = new SocialLinksFetcher();
            for (Candidate c : existingCandidates) {
                existingEmails.add(c.getEmail().trim());
            }

            if (!existingEmails.contains(candidate.getEmail())) {
                candidate.set_id(helper.generateUniqueId());
                candidate = socialLinksFetcher.fetchFullContactApiResults(candidate);
                profileService.saveProfile(candidate);
            } else {
                logger.debug("duplicate profile:::::" + candidate.getEmail());
                apiResult.setError("Duplicate profiles found");
                apiResult.setData(candidate.getEmail());
            }


        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new Gson().toJson(apiResult), HttpStatus.OK);
    }

}
