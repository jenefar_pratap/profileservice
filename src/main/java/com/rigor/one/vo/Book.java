package com.rigor.one.vo;

/**
 * Created by jenefar on 7/8/16.
 */
public class Book {
    private String title;
    private String author;
    private String price;

    public Book() {
    }

    public String toString() {
        return String.format("%s - %s - %f", title, author, price);
    }

    // getters and setters

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
