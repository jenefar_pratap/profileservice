package com.rigor.one.service;

import com.google.gson.Gson;
import com.rigor.one.domain.Candidate;
import com.rigor.one.util.Helper;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jenefar
 */
@Service
public class ProfileServiceImpl implements ProfileService{

    @Autowired
    MongoTemplate mongoTemplate;

    Helper helper=new Helper();

    @Override
    @Transactional
    public List<Candidate> getProfilesByJobId(String jobId) {

        Query query=new Query(Criteria.where("jobId").is(jobId));
        return mongoTemplate.find(query,Candidate.class,"candidates");
    }

    @Override
    public List<Candidate> getProfilesByJobIdAndEmail(String jobId, String email) {
        Query query=new Query(Criteria.where("jobId").is(jobId).and("email").is(email));
        return mongoTemplate.find(query,Candidate.class,"candidates");
    }

    @Override
    public List<String> getCandidateEmailsForJob(String jobId) {

        Query query=new Query(Criteria.where("jobId").is(jobId));
        query.fields().include("_email");

        return mongoTemplate.find(query,String.class,"candidates");
    }

    @Override
    public void saveProfile(Candidate candidate) {
         mongoTemplate.save(candidate);
    }

    @Override
    public List<Candidate> extractProfilesFromExcelTracker(String excelFilePath) {
        FileInputStream fileInputStream=helper.readFile(excelFilePath);

        if(null !=fileInputStream) {
            XSSFWorkbook wb;
            XSSFSheet ws = null;
            try {
                wb = new XSSFWorkbook(fileInputStream);
                ws = wb.getSheetAt(0);
                ws.setForceFormulaRecalculation(true);


            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }


            int rowNum = ws.getLastRowNum() + 1;
            System.out.println("row num:::" + rowNum);
            System.out.println("no::::" + ws.getRow(0));

            int colNum = ws.getRow(1).getLastCellNum();
            int nameHeaderIndex = -1, emailHeaderIndex = -1, contactNumberHeaderIndex = -1, graduationIndex = -1, specializationIndex = -1, totExpIndex = -1, relevantExpIndex = -1, currentCompanyIndex = -1, currentDesignationIndex = -1, cctcIndex = -1, ectcIndex = -1, currentLocIndex = -1, noticePeriodIndex = -1;

            // Read the headers
            XSSFRow rowHeader = ws.getRow(1);
            for (int j = 1; j < colNum; j++) {
                XSSFCell cell = rowHeader.getCell(j);
                String cellValue = helper.cellToString(cell);
                if ("Name".equalsIgnoreCase(cellValue)) {
                    nameHeaderIndex = j;
                } else if ("Graduation".equalsIgnoreCase(cellValue)) {
                    graduationIndex = j;
                } else if ("Contact No".equalsIgnoreCase(cellValue)) {
                    contactNumberHeaderIndex = j;
                } else if ("Specialization".equalsIgnoreCase(cellValue)) {
                    specializationIndex = j;
                } else if ("Total Exp.".equalsIgnoreCase(cellValue)) {
                    totExpIndex = j;
                } else if ("Relevant Exp.".equalsIgnoreCase(cellValue)) {
                    relevantExpIndex = j;
                } else if ("Company".equalsIgnoreCase(cellValue)) {
                    currentCompanyIndex = j;
                } else if ("Designation".equalsIgnoreCase(cellValue)) {
                    currentDesignationIndex = j;
                } else if ("CCTC".equalsIgnoreCase(cellValue)) {
                    cctcIndex = j;
                } else if ("ECTC".equalsIgnoreCase(cellValue)) {
                    ectcIndex = j;
                } else if ("Current Location".equalsIgnoreCase(cellValue)) {
                    currentLocIndex = j;
                } else if ("Notice period".equalsIgnoreCase(cellValue)) {
                    noticePeriodIndex = j;
                } else if ("Email id".equalsIgnoreCase(cellValue)) {
                    emailHeaderIndex = j;
                }
            }

            List<Candidate> candidateList = new ArrayList<Candidate>();
            for (int i = 2; i < rowNum; i++) {
                Candidate candidate = new Candidate();
                XSSFRow row = ws.getRow(i);

                if (null == helper.cellToString(row.getCell(nameHeaderIndex)))
                    break;
                candidate.setFirstName(helper.cellToString(row.getCell(nameHeaderIndex)));
                candidate.setPhoneNumber(helper.cellToString(row.getCell(contactNumberHeaderIndex)));
                candidate.setHighestDegree(helper.cellToString(row.getCell(graduationIndex)));
                candidate.setCurrentCompany(helper.cellToString(row.getCell(currentCompanyIndex)));
                candidate.setCurrentDesignation(helper.cellToString(row.getCell(currentDesignationIndex)));
                candidate.setCurrentLocation(helper.cellToString(row.getCell(currentLocIndex)));
                candidate.setSpecialization(helper.cellToString(row.getCell(specializationIndex)));
                candidate.setEmail(helper.cellToString(row.getCell(emailHeaderIndex)));
                candidate.setNoticePeriod(helper.cellToString(row.getCell(noticePeriodIndex)));
                candidate.setCurrentCTC(helper.cellToString(row.getCell(cctcIndex)));
                candidate.setExpectedCTC(helper.cellToString(row.getCell(ectcIndex)));
                candidate.setTotalExperience(helper.cellToString(row.getCell(totExpIndex)));
                candidate.setRelevantExperience(helper.cellToString(row.getCell(relevantExpIndex)));

                candidateList.add(candidate);

            }

            System.out.println(new Gson().toJson(candidateList));
            return candidateList;
        }
        return null;
    }
}
