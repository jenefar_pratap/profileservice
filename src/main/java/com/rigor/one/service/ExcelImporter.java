package com.rigor.one.service;

import com.fullcontact.api.libs.fullcontact4j.FullContact;
import com.fullcontact.api.libs.fullcontact4j.FullContactException;
import com.fullcontact.api.libs.fullcontact4j.http.person.PersonRequest;
import com.fullcontact.api.libs.fullcontact4j.http.person.PersonResponse;
import com.fullcontact.api.libs.fullcontact4j.http.person.model.SocialProfile;
import com.google.gson.Gson;
import com.rigor.one.domain.Candidate;
import com.rigor.one.util.Helper;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jenefar
 */
public class ExcelImporter {

    Helper helper=new Helper();

    public  List<Candidate> readExcelTracker(String file) {

        FileInputStream fileInputStream=helper.readFile(file);

        if(null !=fileInputStream) {
            XSSFWorkbook wb;
            XSSFSheet ws = null;
            try {
                wb = new XSSFWorkbook(fileInputStream);
                ws = wb.getSheetAt(0);
                ws.setForceFormulaRecalculation(true);


            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }


            int rowNum = ws.getLastRowNum() + 1;
            System.out.println("row num:::" + rowNum);
            System.out.println("no::::" + ws.getRow(0));

            int colNum = ws.getRow(1).getLastCellNum();
            int nameHeaderIndex = -1, emailHeaderIndex = -1, contactNumberHeaderIndex = -1, graduationIndex = -1, specializationIndex = -1, totExpIndex = -1, relevantExpIndex = -1, currentCompanyIndex = -1, currentDesignationIndex = -1, cctcIndex = -1, ectcIndex = -1, currentLocIndex = -1, noticePeriodIndex = -1;

            // Read the headers
            XSSFRow rowHeader = ws.getRow(1);
            for (int j = 1; j < colNum; j++) {
                XSSFCell cell = rowHeader.getCell(j);
                String cellValue = cellToString(cell);
                if ("Name".equalsIgnoreCase(cellValue)) {
                    nameHeaderIndex = j;
                } else if ("Graduation".equalsIgnoreCase(cellValue)) {
                    graduationIndex = j;
                } else if ("Contact No".equalsIgnoreCase(cellValue)) {
                    contactNumberHeaderIndex = j;
                } else if ("Specialization".equalsIgnoreCase(cellValue)) {
                    specializationIndex = j;
                } else if ("Total Exp.".equalsIgnoreCase(cellValue)) {
                    totExpIndex = j;
                } else if ("Relevant Exp.".equalsIgnoreCase(cellValue)) {
                    relevantExpIndex = j;
                } else if ("Company".equalsIgnoreCase(cellValue)) {
                    currentCompanyIndex = j;
                } else if ("Designation".equalsIgnoreCase(cellValue)) {
                    currentDesignationIndex = j;
                } else if ("CCTC".equalsIgnoreCase(cellValue)) {
                    cctcIndex = j;
                } else if ("ECTC".equalsIgnoreCase(cellValue)) {
                    ectcIndex = j;
                } else if ("Current Location".equalsIgnoreCase(cellValue)) {
                    currentLocIndex = j;
                } else if ("Notice period".equalsIgnoreCase(cellValue)) {
                    noticePeriodIndex = j;
                } else if ("Email id".equalsIgnoreCase(cellValue)) {
                    emailHeaderIndex = j;
                }
            }

            List<Candidate> candidateList = new ArrayList<Candidate>();
            for (int i = 2; i < rowNum; i++) {
                Candidate candidate = new Candidate();
                XSSFRow row = ws.getRow(i);

                if (null == cellToString(row.getCell(nameHeaderIndex)))
                    break;
                candidate.setFirstName(cellToString(row.getCell(nameHeaderIndex)));
                candidate.setPhoneNumber(cellToString(row.getCell(contactNumberHeaderIndex)));
                candidate.setHighestDegree(cellToString(row.getCell(graduationIndex)));
                candidate.setCurrentCompany(cellToString(row.getCell(currentCompanyIndex)));
                candidate.setCurrentDesignation(cellToString(row.getCell(currentDesignationIndex)));
                candidate.setCurrentLocation(cellToString(row.getCell(currentLocIndex)));
                candidate.setSpecialization(cellToString(row.getCell(specializationIndex)));
                candidate.setEmail(cellToString(row.getCell(emailHeaderIndex)));
                candidate.setNoticePeriod(cellToString(row.getCell(noticePeriodIndex)));
                candidate.setCurrentCTC(cellToString(row.getCell(cctcIndex)));
                candidate.setExpectedCTC(cellToString(row.getCell(ectcIndex)));
                candidate.setTotalExperience(cellToString(row.getCell(totExpIndex)));
                candidate.setRelevantExperience(cellToString(row.getCell(relevantExpIndex)));

                candidateList.add(candidate);

            }

            System.out.println(new Gson().toJson(candidateList));
            return candidateList;
        }

        return null;
    }
    public static String cellToString(XSSFCell cell) {

        int type;
        Object result = null;
        if(null!=cell) {
            type = cell.getCellType();

            switch (type) {

                case XSSFCell.CELL_TYPE_NUMERIC:
                    result = BigDecimal.valueOf(cell.getNumericCellValue())
                            .toPlainString();

                    break;
                case XSSFCell.CELL_TYPE_STRING:
                    result = cell.getStringCellValue();
                    break;
                case XSSFCell.CELL_TYPE_BLANK:
                    result = "";
                    break;
                case XSSFCell.CELL_TYPE_FORMULA:
                    result = cell.getCellFormula();
            }
            return result.toString();

        }
        return null;
    }

    /*public static void main(String args[]){
        readExcelTracker("/home/jenefar/Downloads/test1xl.xlsx");

        //fetchFullContactApiResults();

    }*/

    private static void fetchFullContactApiResults() {
        FullContact fullContact = FullContact.withApiKey("20505e203dc74a42").build();
        PersonRequest personRequest = fullContact.buildPersonRequest().email("purvaja10@gmail.com").build();
        try {
            PersonResponse personResponse = fullContact.sendRequest(personRequest);
            System.out.println("personResponse::::"+personResponse.toString());
            System.out.println("photo:"+personResponse.getPhotos());
            System.out.println("social profiles"+personResponse.getSocialProfiles());
            Map<String,String> socialProfiles=new HashMap<>();
            for(SocialProfile socialProfile:personResponse.getSocialProfiles())
            {
                socialProfiles.put(socialProfile.getTypeName().toLowerCase(),socialProfile.getUrl());
                System.out.println(socialProfile.getTypeName());
                System.out.println(socialProfile.getUrl());
            }
        } catch (FullContactException e) {
            e.printStackTrace();
        }

    }


}
