package com.rigor.one.service;

import com.fullcontact.api.libs.fullcontact4j.FullContact;
import com.fullcontact.api.libs.fullcontact4j.FullContactException;
import com.fullcontact.api.libs.fullcontact4j.http.person.PersonRequest;
import com.fullcontact.api.libs.fullcontact4j.http.person.PersonResponse;
import com.fullcontact.api.libs.fullcontact4j.http.person.model.SocialProfile;
import com.rigor.one.domain.Candidate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author jenefar
 */
public class SocialLinksFetcher {

   /* public static void main(String args[]){

    }*/
   public Candidate fetchFullContactApiResults(Candidate candidate) {
       FullContact fullContact = FullContact.withApiKey("20505e203dc74a42").build();
       PersonRequest personRequest = fullContact.buildPersonRequest().email(candidate.getEmail()).build();
       try {
           PersonResponse personResponse = fullContact.sendRequest(personRequest);
           System.out.println("personResponse::::"+personResponse.toString());
           System.out.println("photo:"+personResponse.getPhotos());
           System.out.println("social profiles"+personResponse.getSocialProfiles());
           Map<String,String> socialProfiles=new HashMap<>();

           if(personResponse.getPhotos().size()>0){
               candidate.setPhotoUrl(personResponse.getPhotos().get(0).getUrl());
           }
           if(personResponse.getSocialProfiles().size()>0){
               for(SocialProfile socialProfile:personResponse.getSocialProfiles())
               {
                   if(socialProfile.getTypeName().contains("."))
                   {
                      String key= socialProfile.getTypeName().replace(".","");
                       socialProfiles.put(key.toLowerCase(),socialProfile.getUrl());

                   }
                   else{
                       socialProfiles.put(socialProfile.getTypeName().toLowerCase(),socialProfile.getUrl());

                   }
                   System.out.println(socialProfile.getTypeName());
                   System.out.println(socialProfile.getUrl());
               }
               candidate.setSocialProfiles(socialProfiles);

           }
           if(null !=personResponse.getDemographics().getGender()){
               candidate.setGender(personResponse.getDemographics().getGender());
           }
       } catch (FullContactException e) {
           e.printStackTrace();
       }

       return candidate;

   }
}
