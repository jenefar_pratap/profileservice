package com.rigor.one.service;

import com.rigor.one.vo.Book;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author jenefar
 */
public class ProfileServiceTest {

    public static void readDocFile(String fileName) {

        try {
            File file = new File(fileName);
            FileInputStream fis = new FileInputStream(file.getAbsolutePath());

            HWPFDocument doc = new HWPFDocument(fis);

            WordExtractor we = new WordExtractor(doc);

            String[] paragraphs = we.getParagraphText();

            System.out.println("Total no of paragraph "+paragraphs.length);
            for (String para : paragraphs) {
                System.out.println(para.toString());
            }
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void readDocxFile(String fileName) {

        try {
            File file = new File(fileName);
            FileInputStream fis = new FileInputStream(file.getAbsolutePath());

            XWPFDocument document = new XWPFDocument(fis);

            List<XWPFParagraph> paragraphs = document.getParagraphs();

            System.out.println("Total no of paragraph "+paragraphs.size());
            for (XWPFParagraph para : paragraphs) {
                System.out.println("*********************");
                System.out.println(para.getText());
            }
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void readExcelTracker(String excelFilePath){
            List<Book> listBooks = new ArrayList();

        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream(new File(excelFilePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Workbook workbook = null;
        try {
            workbook = new XSSFWorkbook(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Sheet firstSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = firstSheet.iterator();

            while (iterator.hasNext()) {
                Row nextRow = iterator.next();
                Iterator<Cell> cellIterator = nextRow.cellIterator();
                Book aBook = new Book();

                while (cellIterator.hasNext()) {
                    Cell nextCell = cellIterator.next();
                    int columnIndex = nextCell.getColumnIndex();

                    switch (columnIndex) {
                        case 1:
                            System.out.println("1:cell value:::"+getCellValue(nextCell));
                            //aBook.setTitle((String)getCellValue(nextCell));
                            break;
                        case 2:
                            System.out.println("2:cell value:::"+getCellValue(nextCell));

                            //aBook.setAuthor((String) getCellValue(nextCell));
                            break;
                        case 3:
                            System.out.println("3:cell value:::"+getCellValue(nextCell));

                           // aBook.setPrice((String) getCellValue(nextCell));
                            break;
                    }


                }
                listBooks.add(aBook);
            }

        try {
            workbook.close();
            inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

          /* for(Book b:listBooks)
           {
               System.out.println("Book.getPrice:::"+b.getPrice());
           }
*/
    }

    private static Object getCellValue(Cell cell) {
        System.out.println("cell type:::"+cell.getCellType());

        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                return cell.getStringCellValue();

            case Cell.CELL_TYPE_BOOLEAN:
                return cell.getBooleanCellValue();

            case Cell.CELL_TYPE_NUMERIC:
                return cell.getNumericCellValue();
        }

        return null;
    }

    /*public static void readExcel(){
        InputStream inp = null;
        try {
            inp = new FileInputStream("/home/jenefar/Downloads/test1xl.xlsx");

            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);
            Header header = sheet.getHeader();

            int rowsCount = sheet.getLastRowNum();
            System.out.println("Total Number of Rows: " + (rowsCount + 1));
            for (int i = 0; i <= rowsCount; i++) {
                Row row = sheet.getRow(i);
                int colCounts = row.getLastCellNum();
                System.out.println("Total Number of Cols: " + colCounts);
                for (int j = 0; j < colCounts; j++) {
                    Cell cell = row.getCell(j);
                    System.out.println("[" + i + "," + j + "]=" + cell.getStringCellValue());
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();;
            //java.util.logging.Logger.getLogger(FieldController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                inp.close();
            } catch (IOException ex) {
                ex.printStackTrace();
                //java.util.logging.Logger.getLogger(FieldController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }*/
    public static void readExcelTest() {
        File excel = new File("/home/jenefar/Downloads/test1xl.xlsx");
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(excel);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }

        XSSFWorkbook wb;
        XSSFSheet ws=null;
        try {
            wb = new XSSFWorkbook(fis);
            ws = wb.getSheetAt(0);


        } catch (IOException e) {
            e.printStackTrace();
        }

        ws.setForceFormulaRecalculation(true);

        int rowNum = ws.getLastRowNum() + 1;
        System.out.println("row num:::" + rowNum);
        System.out.println("no::::" + ws.getRow(0));

        int colNum = ws.getRow(1).getLastCellNum();
        int nameHeaderIndex = -1,emailHeaderIndex= -1 ,phoneNumberIndex= -1,contactNumberHeaderIndex = -1,graduationIndex= -1,specializationIndex= -1,totExpIndex= -1,relevantExpIndex= -1,currentCompanyIndex= -1,currentDesignationIndex= -1,cctcIndex= -1,ectcIndex= -1,currentLocIndex=-1;

        // Read the headers first. Locate the ones you need
        XSSFRow rowHeader = ws.getRow(1);
        for (int j = 1; j < colNum; j++) {
            XSSFCell cell = rowHeader.getCell(j);
            String cellValue = cellToString(cell);
            if ("Name".equalsIgnoreCase(cellValue)) {
                nameHeaderIndex = j;
            } else if ("Graduation".equalsIgnoreCase(cellValue)) {
                graduationIndex = j;
            } else if ("Contact No".equalsIgnoreCase(cellValue)) {
                contactNumberHeaderIndex = j;
            }
            //Contact No
        }

        /*if (surnameHeaderIndex == -1 || valueHeaderIndex == -1) {
            try {
                throw new Exception("Could not find header indexes\nSurname : "
                        + surnameHeaderIndex + " | Value : " + valueHeaderIndex);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
       // createnew workbook
        XSSFWorkbook workbook = new XSSFWorkbook();
        // Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("data");

        for (int i = 1; i < rowNum; i++) {
            XSSFRow row = ws.getRow(i);
            String surname = cellToString(row.getCell(nameHeaderIndex));
            System.out.println("first value:" + surname);
            String value = cellToString(row.getCell(graduationIndex));
            System.out.println("second value:" + value);
            String contactNumber = cellToString(row.getCell(contactNumberHeaderIndex));
            System.out.println("third value:" + contactNumber);
            int cellIndex = 0;
            //Create a newRow object for the output excel.
            //We begin for i = 1, because of the headers from the input excel, so we go minus 1 in the new (no headers).
            //If for the output we need headers, add them outside this for loop, and go with i, not i-1
            XSSFRow newRow = sheet.createRow(i - 1);
            newRow.createCell(cellIndex++).setCellValue(surname);
            newRow.createCell(cellIndex++).setCellValue(value);
        }

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(new File("test1.xlsx"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            workbook.write(fos);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert fos != null;
        try {
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static String cellToString(XSSFCell cell) {

        int type;
        Object result = null;
        if(null!=cell) {
            type = cell.getCellType();

            switch (type) {

                case XSSFCell.CELL_TYPE_NUMERIC:
                    result = BigDecimal.valueOf(cell.getNumericCellValue())
                            .toPlainString();

                    break;
                case XSSFCell.CELL_TYPE_STRING:
                    result = cell.getStringCellValue();
                    break;
                case XSSFCell.CELL_TYPE_BLANK:
                    result = "";
                    break;
                case XSSFCell.CELL_TYPE_FORMULA:
                    result = cell.getCellFormula();
            }
            return result.toString();

        }
        return null;
    }


   /* public static void main(String[] args) {

        //readDocxFile("/home/jenefar/Documents/resumes/veena-test.docx");

       // readExcel();
       // readDocFile("C:\\Test.doc");
        readExcelTest();
        readExcelTracker("/home/jenefar/Downloads/test1xl.xlsx");

    }
*/
    /*public static void main(String[] args)
    {
        File file = null;
        WordExtractor extractor = null;
        try
        {

            file = new File("/home/jenefar/Documents/resumes/veena-test.docx");
            FileInputStream fis = new FileInputStream(file.getAbsolutePath());
            HWPFDocument document = new HWPFDocument(fis);
            XWP
            extractor = new WordExtractor(document);
            String[] fileData = extractor.getParagraphText();
            for (int i = 0; i < fileData.length; i++)
            {
                if (fileData[i] != null)
                    System.out.println(fileData[i]);
            }
        }
        catch (Exception exep)
        {
            exep.printStackTrace();
        }
    }*/
}
