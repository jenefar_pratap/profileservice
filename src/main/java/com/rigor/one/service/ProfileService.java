package com.rigor.one.service;

import com.rigor.one.domain.Candidate;

import java.util.List;

/**
 * @author jenefar
 */
public interface ProfileService {

    List<Candidate> getProfilesByJobId(String jobId);
    List<Candidate> getProfilesByJobIdAndEmail(String jobId,String email);
    List<String> getCandidateEmailsForJob(String jobId);
    void saveProfile(Candidate candidate);
    List<Candidate> extractProfilesFromExcelTracker(String excelFilePath);

}
