package com.rigor.one.util;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.math.BigDecimal;
import java.net.URI;
import java.util.UUID;

/**
 * @author jenefar
 */
public class Helper {

    private static final Logger logger = LoggerFactory.getLogger(Helper.class);

    public String generateUniqueId(){
        return UUID.randomUUID().toString();
    }

    public static void downloadUrlToFile(URI uri){
        BufferedInputStream inputStream=null;
        try{
            inputStream= new BufferedInputStream(uri.toURL().openStream());
            File file=new File("");
            FileOutputStream e=new FileOutputStream((file));
            IOUtils.copy(inputStream, e);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String uploadFileToLocal(MultipartFile file) {

        String filename = file.getOriginalFilename();
        logger.debug("original file name:"+filename);
        String fileExt = "."+ FilenameUtils.getExtension(file.getOriginalFilename());
        String newFileName=generateUniqueId();

        String directoryName = System.getProperty( "user.home" )+"/tmp";

        File directory = new File(String.valueOf(directoryName));
        if (! directory.exists()){
            directory.mkdir();
        }
        String filePath = directoryName+"/"+newFileName+fileExt;
        logger.debug("File path is::::{}",filePath);

        // Save the file locally
        BufferedOutputStream stream =null;

        try {
            stream = new BufferedOutputStream(new FileOutputStream(new File(filePath)));
            stream.write(file.getBytes());
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return filePath;

    }

    public FileInputStream readFile(String filePath){
        File excel = new File(filePath);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(excel);
            return fis;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String cellToString(XSSFCell cell) {

        int type;
        Object result = null;
        if(null!=cell) {
            type = cell.getCellType();

            switch (type) {

                case XSSFCell.CELL_TYPE_NUMERIC:
                    result = BigDecimal.valueOf(cell.getNumericCellValue())
                            .toPlainString();

                    break;
                case XSSFCell.CELL_TYPE_STRING:
                    result = cell.getStringCellValue();
                    break;
                case XSSFCell.CELL_TYPE_BLANK:
                    result = "";
                    break;
                case XSSFCell.CELL_TYPE_FORMULA:
                    result = cell.getCellFormula();
            }
            return result.toString();

        }
        return null;
    }

}
